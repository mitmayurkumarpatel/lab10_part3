package shape;

/**
 *
 * @author mitpa
 */
public class Rectangle 
{
    private int width;
    private int height;

    //arg consrtuctor
    public Rectangle(int w, int h) {
        this.width = w;
        this.height = h;
    }

    //getter
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    //setter
     public void setWidth(int width) {
        this.width = width;
    }
    public void setHeight(int height) {
        this.height = height;
    }
    
    public int getArea()
    {
        return this.height * this.width;
    }
    
    public final static void setDimensions(Rectangle r, int w, int h)
    {
        r.setWidth(w);
        r.setHeight(h);
        
      //  assert r.getArea() = w * h;
    }
}
