package shape;

/**
 *
 * @author mitpa
 */
import static org.junit.Assert.assertEquals;

public class ShapesDemo {
    
    public static void calculateArea( Rectangle r ) {
        r.setHeight(3);
        r.setWidth(2);
        
        assertEquals(" Area calculation is incorrect ", r.getArea(), 6);
    }
}
    public static void main(String[] args) 
        {
       ShapesDemo.calculateArea (new Rectangle ());
        
       ShapesDemo.calculateArea (new Square ());       
}